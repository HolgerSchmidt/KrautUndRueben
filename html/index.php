<html>
<head>
  <link rel="stylesheet" type="text/css" href="style.css">
  <title>Database</title>
</head>
<body>
  <div class="header">
    <h3>Kraut & Rüben Database</h3>
  </div>
  <form action="" method="post">
  <div class="querys">
    <div class="entity_search">
      <label>Search for:</label>
      <select name="entitys" id="entity-search" onchange="checkElements()">
        <option value="Rezept">Rezept</option>
        <option value="Kunde">Kunde</option>
        <option value="Zutat">Zutat</option>
        <option value="Bestellung">Bestellung</option>
        <option value="Lieferant">Lieferant</option>
        <option value="RezeptZutat">RezeptZutat</option>
      </select>
    </div>
    <div class="recipe_search" id="recipe_search">
      <label>Search after:</label>
      <select name="recipe_search" id="recipe_search">
        <option value="Rezeptname">Rezeptname</option>
        <option value="Ernaehrungskategorie">Ernährungskategorie</option>
        <option value="Zutat">Zutat</option>
        <option value="Kalorienmenge">Kalorienmenge(max)</option>
        <option value="Zutaten">Zutaten(max)</option>
        <option value="Allergien">Allergien</option>
    </select>
    </div>
    <div class="kunde_search" id="kunde_search">
      <label>Search after:</label>
      <select name="kunde_search" id="kunde_search">
        <option value="Nachname">Nachname</option>
    </select>
    </div>
    <div class="zutat_search" id="zutat_search">
      <label>Search after:</label>
      <select name="zutat_search" id="zutat_search">
        <option value="Zutatenname">Zutatenname</option>
        <option value="keinRezept">kein Rezept</option>
    </select>
    </div>
    <div class="bestellung_search" id="bestellung_search">
      <label>Search after:</label>
      <select name="bestellung_search" id="bestellung_search">
        <option value="Bestellnummer">Bestellnummer</option>
    </select>
    </div>
    <div class="lieferant_search" id="lieferant_search">
      <label>Search after:</label>
      <select name="lieferant_search" id="lieferant_search">
        <option value="Lieferantenname">Lieferantenname</option>
    </select>
    </div>
    <div class="recipe_zutat_search" id="recipe_zutat_search">
      <label>Search after:</label>
      <select name="recipe_zutat_search" id="recipe_zutat_search">
        <option value="Rezeptname">Rezeptname</option>
    </select>
    </div>
    <div class="search_bar">
	  <input type="text" id="search_bar" name="search-bar">
	  <input type="submit" value="Search" name="submit">
    </div>
  </div>
  </form>
  <div class="sql_table">
    <h3>SQL-Tabelle</h3>
	  <?php
		// Verbindunsaufbau
		$host = 'db';
		$user = 'root';
		$password = 'itech';
		$db = 'KRAUTUNDRUEBEN';

		$con = new mysqli($host,$user,$password,$db);
		if ($con->connect_error){
		  echo 'Alles zu spaet, Kevin ist tot' . $con->connect_error;
		}
		
		$con->set_charset('utf8');

		// Abfrage
		$table = $_POST["entitys"];
		echo "<h4> Tabelle: \t $table </h4>";

		if ($table == "Rezept"){
			$choice = $_POST["recipe_search"];
			if ($choice == "Rezeptname"){
				$text = $_POST["search-bar"];
				$abfrage = $con->query("SELECT REZEPT.BEZEICHNUNG, REZEPT.NETTOPREIS, REZEPT.KALORIEN, REZEPT.KOHLENHYDRATE, REZEPT.PROTEIN, REZEPT.FETT, REZEPT.BALLASTSTOFFE, REZEPT.NATRIUM FROM REZEPT
										WHERE REZEPT.BEZEICHNUNG LIKE '%" . $text . "%'
										ORDER BY REZEPT.BEZEICHNUNG ASC");
				} else if ($choice == "Ernaehrungskategorie") {
				$text = $_POST["search-bar"];
				$abfrage = $con->query("SELECT REZEPT.BEZEICHNUNG, REZEPT.NETTOPREIS, REZEPT.KALORIEN, REZEPT.KOHLENHYDRATE, REZEPT.PROTEIN, REZEPT.FETT, REZEPT.BALLASTSTOFFE, REZEPT.NATRIUM FROM REZEPT
										INNER JOIN ERNAEHRUNGSKATEGORIEREZEPT ON REZEPT.REZEPTNR=ERNAEHRUNGSKATEGORIEREZEPT.REZEPTNR
										INNER JOIN ERNAEHRUNGSKATEGORIE ON ERNAEHRUNGSKATEGORIEREZEPT.ERNAEHRUNGSKATEGORIEID=ERNAEHRUNGSKATEGORIE.ERNAEHRUNGSKATEGORIEID
										WHERE ERNAEHRUNGSKATEGORIE.ERNAEHRUNGSKATEGORIENAME LIKE '" . $text . "%'
										ORDER BY REZEPT.BEZEICHNUNG ASC");
			} else if ($choice == "Zutat"){
				$text = $_POST["search-bar"];
				$abfrage = $con->query("SELECT REZEPT.BEZEICHNUNG, REZEPT.NETTOPREIS, REZEPT.KALORIEN, REZEPT.KOHLENHYDRATE, REZEPT.PROTEIN, REZEPT.FETT, REZEPT.BALLASTSTOFFE, REZEPT.NATRIUM FROM REZEPT
										INNER JOIN REZEPTZUTAT ON REZEPT.REZEPTNR=REZEPTZUTAT.REZEPTNR
										INNER JOIN ZUTAT ON REZEPTZUTAT.ZUTATENNR=ZUTAT.ZUTATENNR
										WHERE ZUTAT.BEZEICHNUNG LIKE '" . $text . "%'
										ORDER BY REZEPT.BEZEICHNUNG ASC");
			} else if ($choice == "Kalorienmenge"){
				$text = $_POST["search-bar"];
				$abfrage = $con->query("SELECT REZEPT.BEZEICHNUNG, REZEPT.NETTOPREIS, REZEPT.KALORIEN, REZEPT.KOHLENHYDRATE, REZEPT.PROTEIN, REZEPT.FETT, REZEPT.BALLASTSTOFFE, REZEPT.NATRIUM FROM REZEPT
										WHERE REZEPT.KALORIEN<=" . $text . "
										ORDER BY REZEPT.BEZEICHNUNG ASC");
			} else if ($choice == "Zutaten"){
				$text = $_POST["search-bar"];
				$abfrage = $con->query("SELECT REZEPT.BEZEICHNUNG, REZEPT.NETTOPREIS, REZEPT.KALORIEN, REZEPT.KOHLENHYDRATE, REZEPT.PROTEIN, REZEPT.FETT, REZEPT.BALLASTSTOFFE, REZEPT.NATRIUM FROM REZEPT
										WHERE " . $text . " >= (
											SELECT COUNT(REZEPTZUTAT.REZEPTNR) FROM REZEPTZUTAT 
											WHERE REZEPTZUTAT.REZEPTNR=REZEPT.REZEPTNR
											GROUP BY REZEPTZUTAT.REZEPTNR
										)
										ORDER BY REZEPT.BEZEICHNUNG ASC");
			} else if ($choice == "Allergien"){
				$text = $_POST["search-bar"];
				$abfrage = $con->query("SELECT REZEPT.BEZEICHNUNG, REZEPT.NETTOPREIS, REZEPT.KALORIEN, REZEPT.KOHLENHYDRATE, REZEPT.PROTEIN, REZEPT.FETT, REZEPT.BALLASTSTOFFE, REZEPT.NATRIUM FROM REZEPT
										WHERE REZEPT.REZEPTNR != ALL (
											SELECT REZEPTZUTAT.REZEPTNR FROM REZEPTZUTAT 
											INNER JOIN ZUTAT ON REZEPTZUTAT.ZUTATENNR=ZUTAT.ZUTATENNR 
											INNER JOIN ALLERGIENZUTAT ON ALLERGIENZUTAT.ZUTATENNR=ZUTAT.ZUTATENNR 
											INNER JOIN ALLERGIEN ON ALLERGIEN.ALLERGIEID=ALLERGIENZUTAT.ALLERGIEID 
											WHERE ALLERGIEN.ALLERGIENAME LIKE '" . $text . "'
										)
										ORDER BY REZEPT.BEZEICHNUNG ASC");
			}

			// Tabellenausgabe
			echo "
			<table>
			  <tr>
				<th> Bezeichnung </th>
				<th> Nettopreis </th>
				<th> Kalorien </th>
				<th> Kohlenhydrate </th>
				<th> Protein </th>
				<th> Fett </th>
				<th> Ballaststoffe </th>
				<th> Natrium </th>
			  </tr>";

			while($ausgabe = $abfrage->fetch_object()){
			echo "
			  <tr>
				<td> " . $ausgabe->BEZEICHNUNG . " </td>
				<td> " . $ausgabe->NETTOPREIS . " </td>
				<td> " . $ausgabe->KALORIEN . " </td>
				<td> " . $ausgabe->KOHLENHYDRATE . " </td>
				<td> " . $ausgabe->PROTEIN . " </td>
				<td> " . $ausgabe->FETT . " </td>
				<td> " . $ausgabe->BALLASTSTOFFE . " </td>
				<td> " . $ausgabe->NATRIUM . " </td>
			  </tr> ";
			}

			echo "
				</table>";
		} else if ($table == "Kunde"){
			$choice = $_POST["kunde_search"];
			if ($choice == "Nachname"){
				$text = $_POST["search-bar"];
				$abfrage = $con->query("SELECT KUNDE.NACHNAME, KUNDE.VORNAME, KUNDE.GEBURTSDATUM, KUNDE.STRASSE, KUNDE.HAUSNR, KUNDE.ORT, KUNDE.TELEFON, KUNDE.EMAIL FROM KUNDE
								WHERE KUNDE.NACHNAME LIKE '" . $text . "%'
								ORDER BY KUNDE.NACHNAME ASC");
			}

			// Tabellenausgabe
			echo "
			<table>
			  <tr>
				<th> Nachname </th>
				<th> Vorname </th>
				<th> Geburstdatum </th>
				<th> Strasse</th>
				<th> Hausnr </th>
				<th> Ort </th>
				<th> Telefon </th>
				<th> Email </th>
			  </tr>";

			while($ausgabe = $abfrage->fetch_object()){
			echo "
			  <tr>
				<td> " . $ausgabe->NACHNAME . " </td>
				<td> " . $ausgabe->VORNAME . " </td>
				<td> " . $ausgabe->GEBURTSDATUM . " </td>
				<td> " . $ausgabe->STRASSE . " </td>
				<td> " . $ausgabe->HAUSNR . " </td>
				<td> " . $ausgabe->ORT . " </td>
				<td> " . $amysql8usgabe->TELEFON . " </td>
				<td> " . $ausgabe->EMAIL . " </td>
			  </tr> ";
			}

			echo "
				</table>";
		} else if ($table == "Zutat"){
			$choice = $_POST["zutat_search"];
			if ($choice == "Zutatenname"){
				$text = $_POST["search-bar"];
				$abfrage = $con->query("SELECT ZUTAT.BEZEICHNUNG, ZUTAT.NETTOPREIS, ZUTAT.KALORIEN, ZUTAT.KOHLENHYDRATE, ZUTAT.PROTEIN FROM ZUTAT
										WHERE ZUTAT.BEZEICHNUNG LIKE '%" . $text . "%'
										ORDER BY ZUTAT.BEZEICHNUNG ASC");
			} else if ($choice == "keinRezept") {
				$abfrage = $con->query("SELECT ZUTAT.BEZEICHNUNG, ZUTAT.NETTOPREIS, ZUTAT.KALORIEN, ZUTAT.KOHLENHYDRATE, ZUTAT.PROTEIN FROM ZUTAT
										WHERE ZUTAT.ZUTATENNR!= ALL (
											SELECT REZEPTZUTAT.ZUTATENNR FROM REZEPTZUTAT
										)
										ORDER BY ZUTAT.BEZEICHNUNG ASC");
			}

			// Tabellenausgabe
			echo "
			<table>
			  <tr>
				<th> Bezeichnung </th>
				<th> Nettopreis </th>
				<th> Kalorien </th>
				<th> Kohlenhydrate </th>
				<th> Protein </th>
			  </tr>";

			while($ausgabe = $abfrage->fetch_object()){
			echo "
			  <tr>
				<td> " . $ausgabe->BEZEICHNUNG . " </td>
				<td> " . $ausgabe->NETTOPREIS . " </td>
				<td> " . $ausgabe->KALORIEN . " </td>
				<td> " . $ausgabe->KOHLENHYDRATE . " </td>
				<td> " . $ausgabe->PROTEIN . " </td>
			  </tr> ";
			}

			echo "
				</table>";
		} else if ($table == "Bestellung"){
			$choice = $_POST["bestellung_search"];
			if ($choice == "Bestellnummer"){
				$text = $_POST["search-bar"];
				$abfrage = $con->query("SELECT BESTELLUNG.BESTELLNR, BESTELLUNG.KUNDENNR, BESTELLUNG.BESTELLDATUM, BESTELLUNG.RECHNUNGSBETRAG FROM BESTELLUNG
										WHERE BESTELLUNG.BESTELLNR='" . $text . "'
										ORDER BY BESTELLUNG.BESTELLNR ASC");
			}

			// Tabellenausgabe
			echo "
			<table>
			  <tr>
				<th> Bestellnr </th>
				<th> Kundennr </th>
				<th> Bestelldatum </th>
				<th> Rechnungsbetrag </th>
			  </tr>";

			while($ausgabe = $abfrage->fetch_object()){
			echo "
			  <tr>
				<td> " . $ausgabe->BESTELLNR . " </td>
				<td> " . $ausgabe->KUNDENNR . " </td>
				<td> " . $ausgabe->BESTELLDATUM . " </td>
				<td> " . $ausgabe->RECHNUNGSBETRAG . " </td>
			  </tr> ";
			}

			echo "
				</table>";
		} else if ($table == "Lieferant"){
			$choice = $_POST["lieferant_search"];
			if ($choice == "Lieferantenname"){
				$text = $_POST["search-bar"];
				$abfrage = $con->query("SELECT LIEFERANT.LIEFERANTENNAME, LIEFERANT.STRASSE, LIEFERANT.HAUSNR, LIEFERANT.PLZ, LIEFERANT.ORT, LIEFERANT.TELEFON, LIEFERANT.EMAIL FROM LIEFERANT
										WHERE LIEFERANT.LIEFERANTENNAME LIKE '" . $text . "%'
										ORDER BY LIEFERANT.LIEFERANTENNAME ASC");
			}

			// Tabellenausgabe
			echo "
			<table>
			  <tr>
				<th> Lieferantenname </th>
				<th> Strasse</th>
				<th> Hausnr </th>
				<th> Ort </th>
				<th> Telefon </th>
				<th> Email </th>
			  </tr>";

			while($ausgabe = $abfrage->fetch_object()){
			echo "
			  <tr>
				<td> " . $ausgabe->LIEFERANTENNAME . " </td>
				<td> " . $ausgabe->STRASSE . " </td>
				<td> " . $ausgabe->HAUSNR . " </td>
				<td> " . $ausgabe->ORT . " </td>
				<td> " . $ausgabe->TELEFON . " </td>
				<td> " . $ausgabe->EMAIL . " </td>
			  </tr> ";
			}

			echo "
				</table>";
		} else if ($table == "RezeptZutat"){
			$choice = $_POST["recipe_zutat_search"];
			if ($choice == "Rezeptname"){
				$text = $_POST["search-bar"];
				$abfrage = $con->query("SELECT REZEPT.BEZEICHNUNG AS REZBEZ, ZUTAT.BEZEICHNUNG AS ZUTBEZ FROM REZEPT
										LEFT JOIN REZEPTZUTAT ON REZEPT.REZEPTNR=REZEPTZUTAT.REZEPTNR
										INNER JOIN ZUTAT ON REZEPTZUTAT.ZUTATENNR=ZUTAT.ZUTATENNR
										WHERE REZEPT.BEZEICHNUNG LIKE '" . $text . "%' 
										ORDER BY REZEPT.BEZEICHNUNG ASC");
			}

			// Tabellenausgabe
			echo "
			<table>
			  <tr>
				<th> Rezeptname </th>
				<th> Zutatenname</th>
			  </tr>";

			while($ausgabe = $abfrage->fetch_object()){
			echo "
			  <tr>
				<td> " . $ausgabe->REZBEZ . " </td>
				<td> " . $ausgabe->ZUTBEZ . " </td>
			  </tr> ";
			}

			echo "
				</table>";
		}
		?>
  </div>
  <script src="script.js"></script>
</body>
</html>
