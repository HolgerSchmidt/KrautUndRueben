

function checkElements(){

  var e = document.getElementById('entity-search');
  var entity = e.options[e.selectedIndex].text;

  if(entity == "Rezept"){
    disableElements()
    document.getElementById("recipe_search").style.display = "block";
  } else if (entity == "Kunde") {
    disableElements();
    document.getElementById("kunde_search").style.display = "block";
  } else if (entity == "Zutat") {
    disableElements();
    document.getElementById("zutat_search").style.display = "block";
  } else if (entity == "Bestellung"){
    disableElements();
    document.getElementById("bestellung_search").style.display = "block";
  } else if (entity == "Lieferant") {
    disableElements();
    document.getElementById("lieferant_search").style.display = "block";
  } else if (entity == "RezeptZutat") {
    disableElements();
    document.getElementById("recipe_zutat_search").style.display = "block";
  }

}

function disableElements(){
  document.getElementById("recipe_search").style.display = "none";
  document.getElementById("kunde_search").style.display = "none";
  document.getElementById("zutat_search").style.display = "none";
  document.getElementById("bestellung_search").style.display = "none";
  document.getElementById("lieferant_search").style.display = "none";
  document.getElementById("recipe_zutat_search").style.display = "none";

}
