# KrautUndRueben

Kann unter Linux ausgeführt werden.

Installation von Docker auf Ubuntu:
 - https://docs.docker.com/engine/install/ubuntu/

Installation von Docker-Compose:
 - https://docs.docker.com/compose/install/


_Das Workdirectory des Terminals muss der KrautUndRueben-Ordner sein_

Starten tut man die Container mit:
```
sudo docker-compose up
```

Die Scripte müssen noch ausführbar gemacht werden:
```
sudo chmod +x renewDatabase.sh `
sudo chmod +x database/execSqlFiles.sh`
```

Nun kann man mittles des Scripts die beiden Sql-Datein (Dump-Database + testdata) ausführen:
```
sudo ./renewDatabase
```
